import Home from "../views/homeContent/Home"
import GroupUser from "../views/quantri/nhomnguoidung/GroupUser"
import QuyenCoBan from "../views/quantri/quyencoban/QuyenCoBan"
import TaiNguyenUngDung from "../views/quantri/tainguyenungdung/TaiNguyenUngDung"
import AdminApp from '../views/quantri/ungdung/AdminApp'

const router = [
    {
        path: '/',
        element: Home
    },
    {
        path: '/ungdung',
        element: AdminApp
    },
    {
        path: '/nguoidung',
        element: GroupUser
    },
    {
        path: '/quyen',
        element: QuyenCoBan
    },
    {
        path: '/tainguyen',
        element: TaiNguyenUngDung
    },

]
export default router