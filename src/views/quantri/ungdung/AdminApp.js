import React from 'react';
import './AdminApp.scss'
import TopApp from './TopApp';
import SearchBtnApp from './SeachBtnApp'
import Table from './Table';

class AdminApp extends React.Component {
    state = {}
    render() {
        return (
            <div className='container shadow'>
                <TopApp />

                <SearchBtnApp />

                <Table />
            </div>
        );
    }
}

export default AdminApp;