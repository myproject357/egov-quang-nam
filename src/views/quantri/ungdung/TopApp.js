import React from 'react';

class TopApp extends React.Component {
    state = {}
    render() {
        return (
            <div className='d-flex justify-content-between p-4 mt-3'>
                <h4 className='text-uppercase font-blue'>
                    <i className="fa-solid fa-layer-group"></i>
                    &nbsp;Danh sách ứng dụng</h4>
                <button className='custom-btn btn-create-app me-4'>Thêm ứng dụng</button>
            </div>
        );
    }
}

export default TopApp;