import React from 'react';

class SearchBtnApp extends React.Component {
    state = {}
    render() {
        return (
            <div className='col-12 row mt-3'>
                <div className='col-9 col-md-9 col-sm-8'>
                    <div className="input-group mb-3">
                        <input type="text" className="form-control" placeholder="Tìm kiếm ..." />
                        <button className='custom-btn btn-search'><span>dữ liệu</span><span><i className="fa-solid fa-magnifying-glass"></i>Tìm kiếm</span></button>
                    </div>
                </div>
                <div className='col-3 col-md-3 col-sm-4'>
                    <select className="form-select" aria-label="Default select example">
                        <option value="1">Tất cả trạng thái</option>
                        <option value="2">Hiển thị trên Egov Quảng Nam</option>
                        <option value="3">Không hiển thị trên Egov Quảng Nam</option>
                    </select>

                </div>
            </div>
        );
    }
}

export default SearchBtnApp;