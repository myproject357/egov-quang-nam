import React from 'react';
import ImgOne from '../../../assets/images/463d250d47dbc19ea0ce21ecd7d712e5_sm.png'

class Table extends React.Component {
    state = {
        appData: [
            {
                id: 1,
                icon: ImgOne,
                SPDisplayName: "Egov Quảng Nam",
                owner: "Sở Thông tin và Truyền thông",

            },
        ]
    }
    render() {
        let appData = this.state.appData;
        return (
            <div>
                <table className='table'>
                    <thead>
                        <tr className='bg-dark text-white text-center'>
                            <th colSpan="2">ỨNG DỤNG</th>
                            <th>PHIÊN BẢN</th>
                            <th>TRẠNG THÁI</th>
                            <th>HÀNH ĐỘNG</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr className='text-left'>
                            {appData.map((item) => (
                                <>
                                    <td><img
                                        src={item.icon}
                                        style={{ width: '45px', height: "45px" }}
                                    /></td>
                                    <th className=''>
                                        <p className="mb-1">{item.SPDisplayName}</p>
                                        <p className="text-muted">{item.owner}</p>
                                    </th>

                                    <td className='text-center'></td>
                                    <td className='text-center'><i className="fa-solid fa-eye"></i> Hiển thị</td>
                                    <td className='text-center'>
                                        <button type="button" className="btn btn-primary">Cập Nhật</button>
                                        <button type="button" className="btn btn-secondary ms-2">Giám Sát</button>
                                    </td>
                                </>
                            ))}

                        </tr>
                    </tbody>
                </table>
            </div>
        );
    }
}

export default Table;