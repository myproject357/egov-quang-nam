import React from 'react';
import TableUser from './TableUser';

class BtnSearchUser extends React.Component {
    state = {}
    render() {
        return (
            <>
                <div className='col-9'>
                    <div className="input-group mb-3">
                        <input type="text" className="form-control" placeholder="Tìm kiếm ..." />
                        <button className='custom-btn btn-search'><span>dữ liệu</span><span><i className="fa-solid fa-magnifying-glass"></i>Tìm kiếm</span></button>
                        <div className="btn-group" role="group">
                            <button title='Tạo mới' type="button" className="btn btn-primary ms-4"><i className="fa-solid fa-plus"></i></button>
                            <button title='Cấu hình' type="button" className="btn btn-primary"><i className="fa-solid fa-gear"></i></button>
                        </div>
                    </div>
                </div>
                <TableUser />
            </>

        );
    }
}

export default BtnSearchUser;