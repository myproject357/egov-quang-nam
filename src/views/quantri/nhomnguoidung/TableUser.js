import React from 'react';
import './GroupUser.scss'

class TableUser extends React.Component {
    state = {

    }
    render() {
        return (
            <div>
                <table className='table'>
                    <thead>
                        <tr className='bg-dark text-white'>
                            <th className='col-1'>STT</th>
                            <th className='col-11'>Chi tiết</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>1</td>
                            <td>
                                <p className="fw-bold mb-1 p-0 font-table-user">Tên: QLCV - Quản trị đơn vị</p>
                                <p className="fw-normal mb-1 p-0 font-table-user">Mã: QLCV.OrgAdmin</p>
                                <p className="fw-normal p-0 font-table-user">Thứ tự: 0</p>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        );
    }
}

export default TableUser;