import React from 'react';
import './GroupUser.scss'
import BtnSearchUser from './BtnSearchUser';

class GroupUser extends React.Component {
    state = {}
    render() {
        return (
            <div className='container mt-3'>
                <h4 className='text-uppercase font-blue'>
                    <i className="fa-solid fa-layer-group"></i>
                    QUẢN LÝ NHÓM NGƯỜI DÙNG</h4>
                <BtnSearchUser />
            </div >
        );
    }
}

export default GroupUser;