import React from 'react';
import HomeContent from './HomeContent';
import TopHome from './TopHome';

class Home extends React.Component {
    state = {}
    render() {
        return (
            <>
                <div className='container'>
                    <TopHome />
                    <div className='row'>
                        <HomeContent />
                    </div>
                </div>
            </>

        );
    }
}

export default Home;