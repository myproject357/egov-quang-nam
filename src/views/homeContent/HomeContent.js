import React from 'react';
import './HomeContent.scss'
import ImgOne from '../../assets/images/f1de89bc4e7b3262e52b2d656aa6632f_sm.png'
import ImgTwo from '../../assets/images/ab868882a5fb22bbfe73e00fa2d374d0_sm.png'
import ImgThree from '../../assets/images/9ca74c4df5dc89d42e4bd12610571873_sm.png'
import ImgFour from '../../assets/images/41533955efb6bb963e1eb131d8e5db62_sm.png'
import ImgFive from '../../assets/images/f979a0434809a343b96d07796a00d7eb_sm.png'
import ImgSix from '../../assets/images/e3616298147d552862999b54d3ea24c7_sm.png'


class HomeContent extends React.Component {
  state = {
    dataCard: [
      {
        id: 1,
        img: ImgOne,
        text: 'CSDL Cán bộ công chức, viên chức',
        url: 'http://csdl.sonoivu.quangnam.gov.vn',
      },
      {
        id: 2,
        img: ImgTwo,
        text: 'CSDL Khoa học công nghệ',
        url: 'http://csdl.skhcn.quangnam.gov.vn',
      },
      {
        id: 3,
        img: ImgThree,
        text: 'Quản lý văn bản',
        url: 'https://qoffice.quangnam.gov.vn/',
      },
      {
        id: 4,
        img: ImgFour,
        text: 'CSDL Người có công',
        url: 'http://csdlncc.sldtbxh.quangnam.gov.vn',
      },
      {
        id: 5,
        img: ImgFive,
        text: 'Cổng Thông Tin',
        url: "https://cms.quangnam.gov.vn",
      },
      {
        id: 6,
        img: ImgSix,
        text: 'CSDL Ban Dân Tộc',
        url: "http://csdl.bandantoc.quangnam.gov.vn",
      },
    ]

  }
  render() {
    let dataCard = this.state.dataCard
    console.log(dataCard)
    return (
      <>
        {dataCard.map((item) => (
          <div className="col-12 col-sm-6 col-lg-3 mt-4" key={item.id}>
            <div className="d-flex align-items-center custom-card card-flex p-3 h-100 shadow" onClick={() => window.location.href = item.url}>
              <div className="card-img">
                <img src={item.img} alt={item.img} />
              </div>
              <div>
                <h5 className="card-text p-2">
                  <a className='color-text'><span>{item.text}</span></a>
                </h5>
              </div>
            </div>
          </div>
        ))}
      </>
    );
  }
}

export default HomeContent;