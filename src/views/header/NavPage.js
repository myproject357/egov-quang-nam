import React from 'react';
import './Nav.scss'
import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import NavDropdown from 'react-bootstrap/NavDropdown';

class NavPage extends React.Component {
    state = {}
    render() {
        return (
            <Navbar className='topnav d-flex align-items-center' expand="lg">
                <Container>
                    <Navbar.Collapse id="basic-navbar-nav">
                        <Nav className="me-auto">
                            <Nav.Link href="/">
                                <i class="fa-sharp fa-solid fa-house"></i><span>Trang Chủ</span></Nav.Link>
                            <NavDropdown title="Quản Trị" id="basic-nav-dropdown">
                                <NavDropdown.Item href="/ungdung">
                                    <span>Ứng dụng</span>
                                </NavDropdown.Item>
                                <NavDropdown.Item href="/nguoidung">
                                    Nhóm người dùng
                                </NavDropdown.Item>
                                <NavDropdown.Item href="/quyen">
                                    Quyền cơ bản
                                </NavDropdown.Item>
                                <NavDropdown.Item href="/tainguyen">
                                    Tài nguyên ứng dụng
                                </NavDropdown.Item>
                            </NavDropdown>
                            <Nav.Link href="#link">Chuyển đổi số</Nav.Link>
                            <NavDropdown title="Giám sát" id="basic-nav-dropdown">
                                <NavDropdown.Item href="#action/3.1">
                                    Máy Chủ
                                </NavDropdown.Item>
                                <NavDropdown.Item href="#action/3.2">
                                    Ứng dụng
                                </NavDropdown.Item>
                            </NavDropdown>
                        </Nav>
                    </Navbar.Collapse>
                </Container>
            </Navbar>
        );
    }
}

export default NavPage;