import './Header.scss'
import Logo from '../../assets/images/logo.png'
import React from 'react';

class TopHead extends React.Component {
    state = {}
    render() {
        return (
            <div className="page-header-top">
                <div className='header-content'>
                    <div className='ms-2 page-logo'>
                        <img className='ms-5 logo-default' src={Logo}></img>
                        <div className='app-title'>
                            <h2 className='uppercase header-title d-none d-lg-block'>HỆ THỐNG CHÍNH QUYỀN ĐIỆN TỬ</h2>
                            <h2 className='d-block d-lg-none'>Tâm đẹp zai</h2>
                            <h3 className='uppercase header-sub-title d-none d-lg-block'>Tỉnh quảng nam</h3>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default TopHead;