import React from "react";
import './Header.scss'
import NavPage from './NavPage';
import TopdHead from "./TopHead";

class Header extends React.Component {
    render() {
        return (
            <>
                <div className="container-fuild">
                    <TopdHead />

                    <NavPage />

                </div>
            </>
        );
    }
}

export default Header